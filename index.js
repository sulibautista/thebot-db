"use strict";

const
  db = require('./lib/db'),
  schema = require('./lib/db-schema'),
  DatabaseEntity = require('./lib/database-entity'),
  Channel = require('./lib/channel'),
  User = require('./lib/user'),
  ChannelUser = require('./lib/channel-user'),
  TopList = require('./lib/top-list'),
  TwitchResolver = require('./lib/twitch-resolver');

let ret = {
  schema: schema,
  DatabaseEntity: DatabaseEntity,
  Channel: Channel,
  User: User,
  ChannelUser: ChannelUser,
  TopList: TopList,
  TwitchResolver: TwitchResolver
};

module.exports = function(){
  ret.db = db().mongoose;
  return ret;
};

