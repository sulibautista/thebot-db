"use strict";

const
  util = require('util'),
  _ = require('lodash'),
  async = require('async'),

  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('./db.js')(),
  DatabaseEntity = require('./database-entity');

module.exports = ChannelUser;

function ChannelUser(opts, data){
  DatabaseEntity.call(this);

  this.channel = opts.channel;
  this.user = opts.user;

  _.extend(this, data);
}

util.inherits(ChannelUser, DatabaseEntity);
DatabaseEntity.copyStatic(ChannelUser);

const
  ObjectId = db.mongoose.Types.ObjectId,
  model = db.ChannelUser;

ChannelUser.model = model;

/* jshint bitwise: false */
ChannelUser.NORMAL       = 1 << 0;
ChannelUser.FOLLOWER     = 1 << 2;
ChannelUser.SUB          = 1 << 4;
ChannelUser.MOD          = 1 << 6;
ChannelUser.TRUSTED_MOD  = 1 << 8;
ChannelUser.MANAGER      = 1 << 10;
ChannelUser.OWNER        = 1 << 12;
ChannelUser.CUSTOM_GROUP = 1 << 14;

ChannelUser.INTERNAL     = 1 << 30;

ChannelUser.STAFF = ChannelUser.MOD | ChannelUser.TRUSTED_MOD | ChannelUser.MANAGER | ChannelUser.OWNER |
  ChannelUser.INTERNAL;
ChannelUser.TRUSTED_STAFF = ChannelUser.TRUSTED_MOD |ChannelUser.MANAGER | ChannelUser.OWNER | ChannelUser.INTERNAL;
ChannelUser.OWNERS = ChannelUser.MANAGER | ChannelUser.OWNER | ChannelUser.INTERNAL;

ChannelUser.getCacheKey = function(channelId){
  return 'channelUser:' + (channelId instanceof ObjectId? channelId.id : channelId.channel.id);
};

ChannelUser.cacheGet = function(cache, id, callback){
  cache.hget(ChannelUser.getCacheKey(id.channel), id.user.id, callback);
};

ChannelUser.cacheSet = function(cache, channelUser, data, callback){
  cache.hset(ChannelUser.getCacheKey(channelUser.channel.id), channelUser.user.id.id, data, callback);
};

ChannelUser.cacheDel = function(cache, ids, callback){
  if(_.isArray(ids)){
    if(ids.length > 0){
      let userIds = ids.map(function(id){ return id.user.id; });
      cache.hdel(ChannelUser.getCacheKey(ids[0].channel), userIds, callback);
    } else {
      callback(null);
    }
  }
};

ChannelUser.restoreFromCache = function(channelUserData, opts, callback){
  channelUserData.id = {
    channel: new ObjectId(channelUserData.id.channel),
    user: new ObjectId(channelUserData.id.user)
  };

  callback(null, new ChannelUser(opts, channelUserData));
};

ChannelUser.findInDatabase = function(id, opts, callback){
  model.findOne()
    .where('user').equals(id.user)
    .where('channel').equals(id.channel)
    .exec(callback);
};

ChannelUser.setUpFromDatabaseImpl = function(dbChannelUser, opts, callback){
  let chUserData = _.extend(_.pick(dbChannelUser,[
    'groups',
    'customGroups',
    'coins',
    'points'
  ]), {
    primaryKey: dbChannelUser._id
  });

  chUserData.id = {
    channel: dbChannelUser.channel,
    user: dbChannelUser.user
  };

  let chUser = new ChannelUser(opts, chUserData);
  chUser.dirty();

  callback(null, chUser);
};

ChannelUser.createInDatabase = function(id, opts, callback){
  let d = {
    channel: id.channel,
    user: id.user,
    groups: ChannelUser.NORMAL,
    points: {
      count: opts.channel.points.initial
    },
    coins: {
      count: opts.channel.coins.initial
    }
  };

  if(opts.channel.twitch.name.slice(1) === opts.user.twitch.name ||
      opts.channel.twitch.name === opts.user.twitch.name){
    d.groups |= ChannelUser.OWNER;
  }

  model.create(d, callback);
};

ChannelUser.singleQuery = function(id){
  return model.findOne()
    .where('user').equals(id.user)
    .where('channel').equals(id.channel);
};

ChannelUser.setCurrencyCount = function(id, channel, currency, count, cb){
  let setCurrency = {};
  setCurrency[currency + '.count'] = channel.applyCurrencyLimits(currency, count);

  ChannelUser.singleQuery(id)
    .update(setCurrency, cb);
};

/**
 * TODO we need to do this atomically at some point..
 * @param id
 * @param channel
 * @param currency
 * @param delta
 * @param cb
 */
ChannelUser.addCurrency = function(id, channel, currency, delta, cb){
  ChannelUser.singleQuery(id)
    .select(currency)
    .exec(ChannelUser.ensureEntity(function(err, dbChannelUser){
      if(err){
        cb(err);
        return;
      }

      dbChannelUser[currency].count = channel.applyCurrencyLimits(currency, dbChannelUser[currency].count + delta);

      if(delta > 0){
        dbChannelUser[currency].globalCount += delta;
      }

      dbChannelUser.save(cb);
    }));
};

ChannelUser.getCurrencyRank = function(channel, currency, count, cb){
  model
    .where('channel').equals(channel.id)
    .where(currency + '.count').gt(count)
    .where('isSystem').equals(null)
    .count()
    .exec(function(err, count){
      cb(err, count + 1);
    });
};

ChannelUser.findTopCurrencyHolders = function(channel, currency, limit, cb){
  model.find()
    .select(currency + '.count user')
    .populate('user', 'twitch.name')
    .where('channel').equals(channel.id)
    .where('isSystem').equals(null)
    .sort('-' + currency + '.count')
    .limit(limit)
    .exec(cb);
};

Object.defineProperty(ChannelUser.prototype, 'nick', {
  get: function(){
    return this.user.nick;
  }
});

ChannelUser.prototype.cacheChanges = function(cmd, callback){
  let self = this;
  this.commitToCache(redis, function(err){
    if(err){
      cmd.log(err, 'Unable to commit dirty channelUser to cache', { channelUser: self });
    }
    if(callback){
      callback(err);
    }
  });
};

ChannelUser.prototype.filterForCache = function(data){
  delete data.user;
  delete data.channel;
};

ChannelUser.prototype.isNormal = function(){
  return this.isAnyOf(ChannelUser.NORMAL);
};

ChannelUser.prototype.isFollower = function(){
  return this.isAnyOf(ChannelUser.FOLLOWER);
};

ChannelUser.prototype.isSub = function(){
  return this.isAnyOf(ChannelUser.SUB);
};

ChannelUser.prototype.isMod = function(){
  return this.isAnyOf(ChannelUser.MOD);
};

ChannelUser.prototype.isManager = function(){
  return this.isAnyOf(ChannelUser.MANAGER);
};

ChannelUser.prototype.isOwner = function(){
  return this.isAnyOf(ChannelUser.OWNER);
};

ChannelUser.prototype.isAnyOf = function(groups) {
  return this.groups & groups;
};

ChannelUser.prototype.hasAnyCustomGroup = function(testGroups){
  return (this.groups & ChannelUser.CUSTOM_GROUP) && testGroups.any(function(g){
    return this.customGroups.indexOf(g) !== -1;
  }, this);
};

ChannelUser.prototype.singleQuery = function(){
  return ChannelUser.singleQuery(this.id);
};

ChannelUser.prototype.getCurrencyCount = function(currency, cb){
  this.singleQuery()
    .setOptions({ lean: true })
    .select(currency + '.count')
    .exec(ChannelUser.ensureEntity(function(err, dbChannelUser){
      cb(err, dbChannelUser && dbChannelUser[currency].count);
    }));
};

ChannelUser.prototype.getCoins = function(cb){
  this.getCurrencyCount('coins', cb);
};

ChannelUser.prototype.getCurrencyAndRank = function(currency, cb){
  let
    res = null,
    self = this;

  async.waterfall([
    this.getCurrencyCount.bind(this, currency),

    function(count, cb){
      if(_.isFinite(count)){
        res = { count: count };
        ChannelUser.getCurrencyRank(self.channel, currency, count, function(err, rank){
          if(err){
            cb(err);
          } else {
            res.rank = rank;
            cb(null);
          }
        });
      } else {
        cb(null);
      }
    }
  ], function(err){
    cb(err, res);
  });
};

ChannelUser.prototype.setCurrencyCount = function(currency, count, cb){
  ChannelUser.setCurrencyCount(this.id, this.channel, currency, count, cb);
};

ChannelUser.prototype.addCurrency = function(currency, delta, cb){
  ChannelUser.addCurrency(this.id, this.channel, currency, delta, cb);
};

/**
 *
 * @param delta
 * @param callback
 */
ChannelUser.prototype.addCoins = function(delta, cb){
  this.addCurrency('coins', delta, cb);
};