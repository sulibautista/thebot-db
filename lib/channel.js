"use strict";

const
  util = require('util'),
  _ = require('lodash'),

  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('./db.js')(),
  DatabaseEntity = require('./database-entity');
  
module.exports = Channel;

const channelDefaults = {
  coins: {
    initial: 20,
    min: 20,
    max: 9999999999
  },

  points: {
    initial: 0,
    min: 0,
    max: 9999999999
  }
};

function Channel(data){
  DatabaseEntity.call(this);

  _.extend(this, data);
}

util.inherits(Channel, DatabaseEntity);
DatabaseEntity.copyStatic(Channel);

const
  ObjectId = db.mongoose.Types.ObjectId,
  model = db.Channel;

Channel.model = model;

Channel.getCacheKey = function(){
  return 'channel';
};

Channel.cacheGet = function(cache, id, callback){
  cache.hget(Channel.getCacheKey(), id.id, callback);
};

Channel.cacheSet = function(cache, channel, data, callback){
  cache.hset(Channel.getCacheKey(), channel.id.id, data, callback);
};

Channel.restoreFromCache = function(channelData, channelArgs, callback){
  channelData.id = new ObjectId(channelData.id);
  callback(null, new Channel(channelData));
};

Channel.findInDatabase = function(id, channelArgs, callback){
  if(id instanceof ObjectId){
    model.findById(id, callback);
  } else {
    model.findOne()
      .where(channelArgs.resolver.getDbChannelNameKey()).equals(id)
      .exec(callback);
  }
};

Channel.setUpFromDatabaseImpl = function(dbChannel, channelArgs, callback){
  let chData = _.extend({
    id: dbChannel._id,
    state: 'offline',
    isPaused: false,
    pluginState: {},
    loadedPlugins: [],
    tempCommands: []
  },
  _.pick(dbChannel,[
    'coins',
    'points',
    'customGroups',
    'ranks'
  ]));

  chData.twitch = _.pick(dbChannel.twitch, ['id', 'name', 'createdAt']);

  let channel = new Channel(chData);
  channel.dirty();

  callback(null, channel);
};

Channel.createInDatabase = function(name, channelArgs, callback){
  if(_.isString(name)){
    let fields = {};
    fields[channelArgs.resolver.getDbChannelNameKey()] = name;
    model.create(_.extend(fields, channelDefaults), callback);
  } else {
    callback(new Error('name is not a string'));
  }
};

Channel.prototype.cacheChanges = function(cmd, callback){
  let self = this;
  this.commitToCache(redis, function(err){
    if(err){
      cmd.log(err, 'Unable to commit dirty channel to cache', { channel: this });
    }
    if(callback){
      callback(err);
    }
  });
};

Channel.prototype.filterForCache = function(/*data*/){
  //delete data.pluginManager;
};

Channel.prototype.getCommandsKey = function(){
  return Channel.getCacheKey() + ':' + this.id.id + ':commands';
};

Channel.prototype.applyCurrencyLimits = function(currency, count){
  let limits = this[currency];
  return Math.min(Math.max(count, limits.min), limits.max);
};