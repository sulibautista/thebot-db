"use strict";

const
  _ = require('lodash'),
  async = require('async');

module.exports = DatabaseEntity;

function DatabaseEntity(){
  this._dirty = false;
  this.tempStore = {};
}

function functionName(fun) {
  var ret = fun.toString();
  ret = ret.substr('function '.length);
  ret = ret.substr(0, ret.indexOf('('));
  return ret;
}

DatabaseEntity.copyStatic = function(entity){
  entity.entityName = functionName(entity);

  _.defaults(entity, {
    restoreOrCreate: DatabaseEntity.restoreOrCreate,
    restoreOrSetUp: DatabaseEntity.restoreOrSetUp,
    findInDatabaseOrCreateAndSetUp: DatabaseEntity.findInDatabaseOrCreateAndSetUp,
    findInDatabaseOrCreate: DatabaseEntity.findInDatabaseOrCreate,
    loadFromCache: DatabaseEntity.loadFromCache,
    parseAndRestoreFromCache: DatabaseEntity.parseAndRestoreFromCache,
    findInDatabaseAndSetUp: DatabaseEntity.findInDatabaseAndSetUp,
    createInDatabaseAndSetUp: DatabaseEntity.createInDatabaseAndSetUp,
    setUpFromDatabase: DatabaseEntity.setUpFromDatabase,

    ensureEntity: DatabaseEntity.ensureEntity
  });
};

DatabaseEntity.restoreOrCreate = function(cache, id, entityArgs, createArgs, callback){
  let self = this;
  async.waterfall([
    this.restoreOrSetUp.bind(this, cache, id, entityArgs),

    function(reply, cb){
      if(reply){
        cb(null, reply);
      } else {
        self.createInDatabaseAndSetUp(id, createArgs, entityArgs, cb);
      }
    }
  ], callback);
};

DatabaseEntity.restoreOrSetUp = function(cache, id, entityArgs, callback){
  let self = this;
  async.waterfall([
    this.loadFromCache.bind(this, cache, id, entityArgs),

    function(reply, cb){
      if(reply){
        cb(null, reply);
      } else {
        self.findInDatabaseAndSetUp(id, entityArgs, cb);
      }
    }
  ], callback);
};

DatabaseEntity.findInDatabaseOrCreateAndSetUp = function(id, entityArgs, createArgs, callback){
  let self = this;
  async.waterfall([
    this.findInDatabaseAndSetUp.bind(this, id, entityArgs),

    function(reply, cb){
      if(reply){
        cb(null, reply);
      } else {
        self.createInDatabaseAndSetUp(id, createArgs, entityArgs, cb);
      }
    }
  ], callback);
};

DatabaseEntity.findInDatabaseOrCreate = function(id, entityArgs, createArgs, callback){
  let self = this;
  async.waterfall([
    this.findInDatabase.bind(this, id, entityArgs),
    function(reply, cb){
      if(reply){
        cb(null, reply);
      } else {
        self.createInDatabase(id, createArgs, cb);
      }
    }
  ], callback);
};

DatabaseEntity.loadFromCache = function(cache, id, entityArgs, callback){
  let self = this;
  async.waterfall([
    this.cacheGet.bind(this, cache, id),

    function(reply, cb){
      if(reply){
        self.parseAndRestoreFromCache(reply, entityArgs, cb);
      } else {
        cb(null, null);
      }
    }
  ], callback);
};

DatabaseEntity.parseAndRestoreFromCache = function(entityStr, entityArgs, callback){
  let entity;
  try{
    entity = JSON.parse(entityStr);
  } catch(e){
    callback(e);
    return;
  }
  this.restoreFromCache(entity, entityArgs, callback);
};

DatabaseEntity.findInDatabaseAndSetUp = function(id, entityArgs, callback){
  let self = this;
  async.waterfall([
    self.findInDatabase.bind(this, id, entityArgs),

    function(reply, cb){
      if(reply){
        self.setUpFromDatabase(reply, entityArgs, cb);
      } else {
        cb(null, null);
      }
    }
  ], callback);
};

DatabaseEntity.createInDatabaseAndSetUp = function(id, createArgs, entityArgs, callback){
  let self = this;
  async.waterfall([
    function(cb){
      self.createInDatabase(id, createArgs, cb);
    },
    function(reply, cb){
      self.setUpFromDatabase(reply, entityArgs, cb);
    }
  ], callback);
};

DatabaseEntity.setUpFromDatabase = function(dbEntity, entityArgs, callback){
  this.setUpFromDatabaseImpl(dbEntity, entityArgs, function(err, entity){
    if(err){
      callback(err);
    } else {
      entity.dbEntity = dbEntity;
      callback(null, entity);
    }
  });
};

DatabaseEntity.ensureEntity = function(cb){
  let self = this;
  return function(err, entity){
    if(err){
      cb(err);
    } else if(!entity){
      cb(new Error('missing ' + self.entityName + ' database entity'));
    } else {
      cb(null, entity);
    }
  };
};

/**
 *
 * @param id
 * @param entityArgs
 */
DatabaseEntity.getCacheKey = function(/*id, entityArgs*/){
  throw new Error('stub');
};

/**
 *
 * @param cache
 * @param id
 * @param entityArgs
 * @param callback
 */
DatabaseEntity.cacheGet = function(/*cache, id, entityArgs, callback*/){
  throw new Error('stub');
};

/**
 *
 * @param cache
 * @param entity
 * @param callback
 */
DatabaseEntity.cacheSet = function(/*cache, entity, data, callback*/){
  throw new Error('stub');
};

/**
 *
 * @param entity
 * @param entityArgs
 * @param callback
 */
DatabaseEntity.restoreFromCache = function(/*entity, entityArgs, callback*/){
  throw new Error('stub');
};

/**
 *
 * @param id
 * @param entityArgs
 * @param callback
 */
DatabaseEntity.findInDatabase = function(/*id, entityArgs, callback*/){
  throw new Error('stub');
};

/**
 *
 * @param dbEntity
 * @param entityArgs
 * @param callback
 */
DatabaseEntity.setUpFromDatabaseImpl = function(/*dbEntity, entityArgs, callback*/){
  throw new Error('stub');
};

/**
 *
 * @param id
 * @param createArgs
 * @param callback
 */
DatabaseEntity.createInDatabase = function(/*id, createArgs, callback*/){
  throw new Error('stub');
};

DatabaseEntity.prototype.getCacheKey = function(){
  return this.constructor.getCacheKey(this.id);
};

/**
 * Puts the entity in the dirty state.
 */
DatabaseEntity.prototype.dirty = function(){
  this._dirty = true;
};

/**
 * Returns whether the entity is dirty.
 * @returns {boolean}
 */
DatabaseEntity.prototype.isDirty = function(){
  return this._dirty;
};

DatabaseEntity.prototype.toCacheValue = function(){
  let data = _.omit(this, 'tempStore', 'dbEntity');
  this.filterForCache(data);
  return JSON.stringify(data);
};

/**
 * Commits all changes to the cache.
 * @param cache
 * @param callback
 */
DatabaseEntity.prototype.commitToCache = function(cache, callback){
  if(this._dirty){
    this._dirty = false;
    this.constructor.cacheSet(cache, this, this.toCacheValue(), callback);
  } else {
    callback(null);
  }
};