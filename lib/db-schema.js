"use strict";

/**
 * Definition of database schemas.
 * The first time is called it loads the schemas into the specified moongose database.
 * Subsequent calls do nothing.
 *
 * @param {Mongoose} db
 */
module.exports = function(db){
  const Schema = db.Schema;

  /**
   * The Channel schema.
   * Stores all information related to a Channel, which is the place where most of the commands operate in.
   * In concrete, this is used a an IRC channel model, but we don't have any assumptions it is one.
   *
   * A channel is owned by a user, but many users can have power to modify and operate the channel.
   */
  const channelSchema = new Schema({
    /**
     * The ID of the channel.
     */
    //_id: Schema.Types.ObjectId,

    /**
     * Whether to track follower status for each channel user.
     * This is an expensive operation in terms of Twitch API calls, therefore we only allow this in
     * a per costumer basis.
     */
    trackFollowers: { type: Boolean, default: false },

    twitch: {
      _id: false,

      /**
       * The id withing Twitch.
       */
      id: Number,

      /**
       * The name of the channel in Twitch.
       * This is the same as the Twitch user's name, plus a '#' at the beginning.
       */
      name: { type: String, lowercase: true },

      createdAt: Date,

      updatedAt: Date,

      streamKey: String,

      /**
       * The teams the channel is member of.
       * TODO Complete channel.twitch.teams schema
       */
      teams: [],

      mature: Boolean,
      banner: String,
      videoBanner: String,
      background: String,
      logo: String,
      email: String,
      login: String,
      url: String
    },

    /**
     * The coin currency configuration.
     */
    coins: {
      _id: false,

      /**
       * Minimum amount of coins users are allowed to have.
       */
      min: Number,

      /**
       * The initial amount of coins users are created with.
       */
      initial: Number,

      /**
       * Maximum amount of coins users are allowed to have.
       */
      max: Number,

      ranks: [
        {
          _id: false,
          range: { start: Number, end: Number },
          title: String
        }
      ]
    },

    points: {
      _id: false,

      /**
       * Minimum amount of points users are allowed to have.
       */
      min: Number,

      /**
       * The initial amount of points users are created with.
       */
      initial: Number,

      /**
       * Maximum amount of points users are allowed to have.
       */
      max: Number
    },

    /**
     * The list of custom user groups.
     * Used for authorization purposes. We use an ObjectId for the id field in order to ensure security when
     * likely named groups are deleted and recreated.
     */
    customGroups: [
      {
        _id: false,
        id: Schema.Types.ObjectId,
        title: String,
        description: String
      }
    ],

    /**
     * The list of command configuration and overrides.
     * Most commands are not required to have any specific configuration, but the user can decide to set some
     * options for specific commands at any time, we store these options here, in a per-command basis. We do
     * not store command's plugin configuration, since plugins are an implementation detail.
     */
    commands: [
      {
        _id: false,

        /**
         * The id of the command, this is the same as declared inside plugins.
         * Since all commands are required to have an unique id accross plugins, we do not need to store plugin
         * ownership information.
         */
        id: { type: String, lowercase: true },

        /**
         * Specifies if the command is disabled for this channel, disabled commands cannot be executed by anyone,
         * except the system.
         */
        disabled: Boolean,

        /**
         * List of string overrides.
         * Most commands have feedback and output strings, which can be overridden here.
         */
        strings: [
          {
            _id: false,
            id: { type: String, lowercase: true },
            text: String
          }
        ],

        /**
         * The basic authorization of the command.
         * This is a bitflag set of user groups. Any user who belongs to any of the groups in the flag set will
         * be able to execute the command.
         * All commands define its initial authorization in their respective plugin file, which can be overridden
         * here.
         */
        auth: { type: Number, min: 0 },

        /**
         * The list of {@link channelSchema.customGroups}
         * In addition to basic auth, users in any of these groups will also be able to execute the command.
         */
        groups: [ Schema.Types.ObjectId ],

        /**
         * Any extra configuration data commands need to persist.
         * Most operational data is stored in Redis or custom collections, however for simple configuration data that
         * needs to be persisted and accessed only on startup, this is the place.
         */
        data: {}
      }
    ],

    /**
     * The configured command timers.
     * Since timers are not used a lot, we can store them as subdocuments.
     */
    commandTimers: [
      {
        _id: false,
        interval: { type: Number, min: 60 * 1000, max: 60 * 60 * 1000 },
        command: String,
        args: {},
        requirements: []
      }
    ]
  });

  channelSchema.index({ 'twitch.name': 1 }, { unique: true });

  // Make name an alias of twitch.name for now
  channelSchema.virtual('name')
    .get(function(){ return this.twitch.name; })
    .set(function(name){ this.twitch.name = name; });


  /**
   * The User schema.
   * Stores viewers, streamers and in general all of the client's global information.
   */
  const userSchema = new Schema({
    /**
     * The ID of the viewer.
     * We use a unique id instead of the normalized Twitch username (which is also unique) for future-proofness.
     */
    //_id: Schema.Types.ObjectId,

    /**
     * The user's real name.
     */
    name: String,

    /**
     * The user's preferred email.
     */
    email: String,

    /**
     * The user's password.
     * Will most likely be null for most users, since Twitch oauth integration will be the preferred auth method.
     */
    password: String,

    /**
     * Whether the user is part of the bot staff, i.e. has god mode on.
     */
    isStaff: Boolean,

    /**
     * Whether the user is a managing admin, i.e. has a few extra permissions in all channels.
     */
    isAdmin: Boolean,

    /**
     * The default channel to show in the UI.
     */
    defaultChannel: { type: Schema.Types.ObjectId, ref: 'Channel' },

    /**
     * The display name of the user.
     * If specified, we use this instead of the Twitch displayName to talk to the user.
     */
    globalDisplayName: String,

    /**
     * The global amount of points of the user.
     */
    globalPoints: Number,

    /**
     * The user's Twitch account.
     */
    twitch: {
      _id: false,

      /**
       * The id withing Twitch.
       */
      id: Number,

      /**
       * The normalized twitch user name.
       * This is the same as the channel the user owns (minus the initial '#').
       */
      name: { type: String, lowercase: true },

      displayName: String,

      createdAt: Date,

      updatedAt: Date,

      oauthToken: String,

      oauthScopes: [String],

      isStaff: Boolean,

      isAdmin: Boolean,

      isTurbo: Boolean,

      isBroadcaster: Boolean,

      isPartner: Boolean,

      lastBroadcast: Date,

      logo: String,

      bio: String,

      email: String
    },

    /**
     * The list of active services for this user.
     * This holds the list services we provide. For most of the users, this is 'botclient' which means
     * the bot can interact with them, if a user doesn't has this service, its globally banned from all of the
     * bots channels.
     *
     * The other service is 'bothost', which allows the user to use the bot in her channel.
     *
     * TODO Rethink most of this, we need more information
     */
    activeServices: [
      {
        _id: false,
        service: { type: String, lowercase: true },
        starts: { type: Date, default: Date.now },
        ends: Date,
        data: {}
      }
    ],

    inactiveServices: [
      {
        _id: false,
        service: { type: String, lowercase: true },
        started: Date,
        ended: Date,
        data: {}
      }
    ]
  });
  // Sparse will be needed if we use other providers in addition to twitch
  userSchema.index({ 'twitch.name': 1 }, { unique: true/*, sparse: true */});
  userSchema.index({ 'twitch.id': 1 }, { sparse: true });
  //userSchema.index({ 'activeServices.service': 1 });


  /**
   * A helper definition for currencies since they all share the same properties.
   */
  const userCurrencyHelperSchema = {
    _id: false,

    globalCount: { type: Number, min: 0, default: 0 },

    /**
     * The amount of currency the user currently has in the channel.
     */
    count: { type: Number, min: 0, default: 0 },

    oldRank: { type: Number, min: -1 },

    cuts: [
      {
        _id: false,
        date: Date,
        count: Number,
        rank: Number
      }
    ],

    globalCuts: [
      {
        _id: false,
        date: Date,
        count: Number,
        rank: Number
      }
    ]
  };


  /**
   * The ChannelUserSchema.
   * Stores all data related to a user interaction in a specific channel.
   */
  const channelUserSchema = new Schema({
    //_id: Schema.Types.ObjectId,

    /**
     * The user the entry refers to.
     */
    user: { type: Schema.Types.ObjectId, ref: 'User' },

    /**
     * The channel the entry refers to.
     */
    channel: { type: Schema.Types.ObjectId, ref: 'Channel' },

    owner: {
      _id: false,
      created: Date
    },

    manager: {
      _id: false,
      created: Date
    },

    mod: {
      created: Date
      //updated: Date
    },

    subscriber: {
      created: Date,
      updated: Date
    },

    follower: {
      _id: false,
      created: Date,
      updated: Date
    },

    /**
     * The group bitflag set of the user in the channel.
     * That is, the user level (mod, staff, etc) on this channel.
     */
    groups: { type: Number, min: 0 },

    /**
     * The custom groups of the user in the channel.
     */
    customGroups: [ Schema.Types.ObjectId ],

    /**
     * Points currency used as a loyalty system.
     */
    points: userCurrencyHelperSchema,

    /**
     * Coins currency used for bets and challanges.
     */
    coins: userCurrencyHelperSchema
  });

  channelUserSchema.index({ 'user' : 1, 'channel': 1 }, { unique: true });
  // Index currency as they are sorted a lot
  channelUserSchema.index({ 'channel': 1, 'mod.created': 1 }, { sparse: true });
  // TODO check if we can use a simpler index
  channelUserSchema.index({ 'channel': 1, 'coins.count': -1 });
  channelUserSchema.index({ 'channel': 1, 'points.count': -1 });


  /**
   * The TopList Schema.
   * A top list is a collection of snapshots of the top users, measured by currecny. These can be used to maintain
   * a list of entries (in the provided ranges) to a given contest.
   * @see {plugins.toplist}
   */
  const topListSchema = new Schema({
    //_id: Schema.Types.ObjectId,

    channel: { type: Schema.Types.ObjectId, ref: 'Channel' },

    ranges: [
      {
        _id: false,
        start: Number,
        end: Number
      }
    ],

    // This should be [ [ { type: Schema.Types.ObjectId, ref: 'ChannelUser' } ] ], but mongoose doesn't support it yet
    snapshots: [],

    ended: Date
  });

  topListSchema.index({ channel: 1 });


  /**
   * The BettingContest schema.
   * @see {plugins.bets}
   */
  const bettingContestSchema = new Schema({
    //_id: Schema.Types.ObjectId,

    channel: { type: Schema.Types.ObjectId, ref: 'Channel' },

    started: Date,

    type: String,

    teams: [String],

    bets: [
      {
        _id: false,
        user: { type: Schema.Types.ObjectId, ref: 'ChannelUser' },
        team: String,
        delta: Number
      }
    ],

    winner: String,

    score: Number,

    bonus: Number,

    deleted: Boolean
  });

  topListSchema.index({ channel: 1 });
  topListSchema.index({ 'bets.user': 1 });

  const exp = module.exports;

  exp.Channel = db.model('Channel', channelSchema);
  exp.User = db.model('User', userSchema);
  exp.ChannelUser = db.model('ChannelUser', channelUserSchema);
  exp.TopList = db.model('TopList', topListSchema);
  exp.BettingContest = db.model('BettingContest', bettingContestSchema);

  exp.loaded = true;
};