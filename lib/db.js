"use strict";

const
  mongoose = require('mongoose'),
  config = require('thebot-conf'),
  schema = require('./db-schema.js');
   
module.exports = function(){
  if(schema.loaded){
    return schema;
  }

  mongoose.connect(config.get('mongo.uri'));
  schema(mongoose);
  
  schema.mongoose = mongoose;
  
  return schema;
};