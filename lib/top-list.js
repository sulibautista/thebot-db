"use strict";

const
  db = require('./db')();

module.exports = TopList;

function TopList(){}

const model = db.TopList;

TopList.model = model;

TopList.getLastActive = function(channel, callback){
  TopList.getActiveQuery(channel).exec(callback);
};

TopList.getActiveQuery = function(channel){
  let q = model.findOne()
    .where('ended').equals(null)
    .sort({ _id: -1 });

  if(channel){
    q.where('channel').equals(channel.id);
  }

  return q;
};