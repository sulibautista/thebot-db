"use strict";

const
  _  = require('lodash');

module.exports = TwitchResolver;

function TwitchResolver(){
  this.channelCache = {};
}

TwitchResolver.prototype.name = 'twitch';

TwitchResolver.prototype.getChannelMapCacheKey = function(){
  return 'channelIdMap:' + this.name;
};

TwitchResolver.prototype.getUserMapCacheKey = function(){
  return 'userIdMap:' + this.name;
};

TwitchResolver.prototype.getChannelId = function(channelName){
  return this.channelCache[channelName];
};

TwitchResolver.prototype.setChannelId = function(chanelName, channelId){
  this.channelCache[chanelName] = channelId;
};

TwitchResolver.prototype.getDbChannelNameKey = function(){
  return this.name + '.name';
};

TwitchResolver.prototype.getDbUserNameKey = function(){
  return this.name + '.name';
}

TwitchResolver.prototype.getUserId = function(/*userName*/){
  return null;
};

TwitchResolver.prototype.setUserId = function(/*userName, userId*/){
  // noop
};