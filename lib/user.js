"use strict";

const
  util = require('util'),
  _ = require('lodash'),
  async = require('async'),
  
  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('./db.js')(),
  DatabaseEntity = require('./database-entity');

module.exports = User;

const defaultFields = {
  activeServices: [{ service: 'bot-user' }]
};

function User(opts, data){
  DatabaseEntity.call(this);

  _.extend(this, data);
}

const
  ObjectId = db.mongoose.Types.ObjectId,
  model = db.User;

User.model = model;

util.inherits(User, DatabaseEntity);
DatabaseEntity.copyStatic(User);

User.getCacheKey = function(){
  return 'user';
};

User.cacheGet = function(cache, id, callback){
  cache.hget(User.getCacheKey(), id.id, callback);
};

User.cacheSet = function(cache, user, data, callback){
  cache.hset(User.getCacheKey(), user.id.id, data, callback);
};

User.restoreFromCache = function(userData, opts, callback){
  userData.id = new ObjectId(userData.id);
  callback(null, new User(opts, userData));
};

User.findInDatabase = function(id, opts, callback){
  if(id instanceof ObjectId){
    model.findById(id, callback);
  } else {
    model.findOne()
      .where(opts.resolver.getDbUserNameKey()).equals(id)
      .exec(callback);
  }
};

User.setUpFromDatabaseImpl = function(dbUser, opts, callback){
  let userData = _.extend(_.pick(dbUser, [
    'name',
    'email',
    'password',
    'isStaff',
    'isAdmin',
    'defaultChannel',
    'globalDisplayName',
    'globalPoints',
    'twitch',
    'activeServices',
    'inactiveServices'
  ]), {
    id: dbUser._id
  });

  let user = new User(opts, userData);
  user.dirty();

  callback(null, user);
};

User.createInDatabase = function(name, opts, callback){
  if(_.isString(name)){
    let fields = {
      twitch: {
        name: name
      }
    };
    model.create(_.extend(fields, defaultFields), callback);
  } else {
    callback(new Error('name is not a string'));
  }
};

Object.defineProperty(User.prototype, 'nick', {
  get: function(){
    return this.globalDisplayName || this.twitch.displayName || this.twitch.name;
  }
});

User.prototype.cacheChanges = function(cmd, callback){
  let self = this;
  this.commitToCache(redis, function(err){
    if(err){
      cmd.log(err, 'Unable to commit dirty user to cache', { user: self });
    }
    if(callback){
      callback(err);
    }
  });
};

User.prototype.filterForCache = function(/*data*/){

};