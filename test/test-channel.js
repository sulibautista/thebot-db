"use strict";

const
  _ = require('lodash'),
  expect = require('chai').expect,
  
  conf = require('thebot-conf'),
  redis = conf.redis(),
  db = require('../lib/db.js')(),
  PluginManager = require('../lib/plugin-manager.js'),
  Channel = require('../lib/channel.js'),
  TwitchResolver = require('../lib/twitch-resolver');

describe('Channel', function(){
  let
    chName = '#~test-channel',
    chFields = { twitch: { name: chName } },
    chOpts = { resolver: new TwitchResolver() },
    chQuery = { 'twitch.name': chName },
    pluginManager = new PluginManager();

  before(function(done){
    pluginManager.loadPlugins(__dirname + '/command-mocks', done);
  });

  describe('#Channel', function(){
    it('should inherit DatabaseEntity');
  });

  describe('#createInDatabase', function(){

    afterEach(function removeTestChannel(done){
      db.Channel.remove(chQuery, done);
    });
      
    it('should create a channel entry in the database', function(done){
      Channel.createInDatabase(chName, chOpts, function(err, dbChannel){
        expect(err).to.not.exist;
        expect(dbChannel).to.have.deep.property('twitch.name', chName);
        
        db.Channel.findOne(chQuery, function(err, channel){
          expect(err).to.not.exist;
          expect(channel).to.have.deep.property('twitch.name', chName);
          done();
        });
      });
    });
  });
  
  describe('#setUpFromDatabase', function(){
    let dbChannel;
    
    function createTestChannel(done){
      Channel.createInDatabase(chName, chOpts, function(err, c){
        dbChannel = c;
        done(err);
      });
    }
    
    function removeTestChannel(done){
      db.Channel.remove(chQuery, done);
    }
  
    beforeEach(createTestChannel);
    afterEach(removeTestChannel);
    
    it('should create a proper Channel instance', function(){
      /*Channel.setUpFromDatabase(dbChannel, pluginManager, function(err, channel){
        expect(err).to.not.exist;
        expect(channel).to.be.an.instanceof(Channel);
        expect(channel).to.have.property('id', chId);
        //expect(channel).to.have.property('commandsKey', 'channel:' + chId + ':commands');
        expect(channel).to.have.property('tempStore').that.is.an('object');
        expect(channel).to.have.property('pluginManager', pluginManager);
        done();
      });*/
    });
    
    it('should mark the created channel as dirty', function(){
      /*Channel.setUpFromDatabase(dbChannel, pluginManager, function(err, channel){
        expect(err).to.not.exist;
        expect(channel).to.have.property('isDirty', true);
        done();
      });*/
    });
    
    it('should create a Channel in offline state by default', function(){
      /*Channel.setUpFromDatabase(dbChannel, pluginManager, function(err, channel){
        expect(err).to.not.exist;
        expect(channel).to.have.property('state', 'offline');
        done();
      });*/
    });
  });
  
  describe('#execCommand', function(){
    /*let channel;
    
    function createTestChannel(done){
      Channel.dbCreate(chId, function(err, dbChannel){
        expect(err).to.not.exist;
        Channel.setUp(dbChannel, pluginManager, function(err, c){
          channel = c;
          done(err);
        });
      });
    }
    
    function removeTestChannel(done){
      let d = _.after(2, done);
      db.Channel.remove({ _id: chId }, d);
      redis.del('channel:' + chId, d);
    }
  
    beforeEach(createTestChannel);
    afterEach(removeTestChannel);
      
    it('should fail to execute a non-existant command', function(done){
      channel.execCommand({id: 'non_existant'}, function(err){
        expect(err).to.have.property('message', 'BOT_CMD_UNKNOWN');
        done();
      });
    });
    
    it('should fail to execute out-of-state command', function(done){
      channel.state = 'paused';
      channel.execCommand({id: 'testCmd' }, function(err){
        expect(err).to.have.property('message', 'BOT_CMD_NOT_AVAILABLE');
        done();
      });
    });
    
    it('should fail to execute a disabled command', function(done){
      channel.disabledCommands.push('testCmd');
      channel.execCommand({id: 'testCmd'}, function(err){
        expect(err).to.have.property('message', 'BOT_CMD_DISABLED');
        done();
      });
    });
    
    it('should execute a valid command', function(done){
      channel.execCommand({id: 'testCmd', user: 'test'}, function(err, res){
        expect(err).to.not.exist;
        expect(res).to.property('output', 'testPlugin.testCmd.exec');
        done();
      });
    });
    
    it('should persist changes applied by commands in redis', function(done){
      channel.execCommand({id: 'testDirtyCmd', user: 'test'}, function(err, res){
        expect(err).to.not.exist;
        expect(res).to.have.property('dirty', 'dirty');
        expect(channel).to.have.property('newDirtyProp', true);
        
        redis.get('channel:' + chId, function(err, data){
          expect(err).to.not.exist;
          let storedChannel = JSON.parse(data);
          expect(storedChannel).to.have.property('newDirtyProp', true);
          done();
        });
      });
    });*/
  });
});