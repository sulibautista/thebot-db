"use strict";

const
  expect = require('chai').expect,
  db = require('../lib/db.js')();
    
describe('Database schema', function(){

  it('should export User, Channel, ChannelUser, TopList and BettingContest models', function(){
    expect(db.User).to.exist;
    expect(db.Channel).to.exist;
    expect(db.ChannelUser).to.exist;
    expect(db.TopList).to.exist;
    expect(db.BettingContest).to.exist;
  });
    
});